-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- CREATE DATABASE "bdchkinchkout" -------------------------
CREATE DATABASE IF NOT EXISTS `bdchkinchkout` CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `bdchkinchkout`;
-- ---------------------------------------------------------


-- CREATE TABLE "tbinout" ----------------------------------
CREATE TABLE `tbinout` ( 
	`idinout` VarChar( 255 ) NOT NULL,
	`cedchkprof` VarChar( 255 ) NOT NULL,
	`diachkprof` VarChar( 255 ) NOT NULL,
	`horachkprof` VarChar( 255 ) NOT NULL,
	`registrochk` VarChar( 255 ) NOT NULL,
	`fechainout` VarChar( 255 ) NOT NULL,
	`operador` VarChar( 255 ) NOT NULL )
ENGINE = InnoDB;
-- ---------------------------------------------------------


-- CREATE TABLE "tbprofe" ----------------------------------
CREATE TABLE `tbprofe` ( 
	`idprof` VarChar( 255 ) NOT NULL,
	`cedulaprof` VarChar( 255 ) NOT NULL,
	`apenomprof` VarChar( 255 ) NOT NULL,
	`fotoprof` VarChar( 255 ) NOT NULL,
	`sexoprof` VarChar( 255 ) NOT NULL,
	`fnacprof` VarChar( 255 ) NOT NULL,
	`lugnacprof` VarChar( 255 ) NOT NULL,
	`correoprof` VarChar( 255 ) NOT NULL,
	`niveleducprof` VarChar( 255 ) NOT NULL,
	`cargoprof` VarChar( 255 ) NOT NULL,
	`gradoprof` VarChar( 255 ) NOT NULL,
	`materiaprof` VarChar( 255 ) NOT NULL,
	`tlfnocasprof` VarChar( 255 ) NOT NULL,
	`tlfnocelprof` VarChar( 255 ) NOT NULL,
	`direccionprof` VarChar( 255 ) NOT NULL,
	`statusprof` VarChar( 255 ) NOT NULL,
	`fechaprof` VarChar( 255 ) NOT NULL,
	`operador` VarChar( 255 ) NOT NULL )
ENGINE = InnoDB;
-- ---------------------------------------------------------


-- Dump data of "tbinout" ----------------------------------
INSERT INTO `tbinout`(`idinout`,`cedchkprof`,`diachkprof`,`horachkprof`,`registrochk`,`fechainout`,`operador`) VALUES ( '1', '12514148', '2016-04-06', '1459976269', 'ENTRADA', '2016-04-06', 'operador' );
INSERT INTO `tbinout`(`idinout`,`cedchkprof`,`diachkprof`,`horachkprof`,`registrochk`,`fechainout`,`operador`) VALUES ( '2', '11857435', '2016-04-06', '1459976283', 'SALIDA', '2016-04-06', 'operador' );
INSERT INTO `tbinout`(`idinout`,`cedchkprof`,`diachkprof`,`horachkprof`,`registrochk`,`fechainout`,`operador`) VALUES ( '3', '19838394', '2016-04-08', '1460122900', 'SALIDA', '2016-04-08', 'operador' );
INSERT INTO `tbinout`(`idinout`,`cedchkprof`,`diachkprof`,`horachkprof`,`registrochk`,`fechainout`,`operador`) VALUES ( '4', '19838394', '2016-04-08', '1460122909', 'ENTRADA', '2016-04-08', 'operador' );
INSERT INTO `tbinout`(`idinout`,`cedchkprof`,`diachkprof`,`horachkprof`,`registrochk`,`fechainout`,`operador`) VALUES ( '5', '12514148', '2016-04-08', '1460136435', 'ENTRADA', '2016-04-08', 'operador' );
INSERT INTO `tbinout`(`idinout`,`cedchkprof`,`diachkprof`,`horachkprof`,`registrochk`,`fechainout`,`operador`) VALUES ( '6', '19838394', '2016-04-11', '1460395745', 'ENTRADA', '2016-04-11', 'operador' );
INSERT INTO `tbinout`(`idinout`,`cedchkprof`,`diachkprof`,`horachkprof`,`registrochk`,`fechainout`,`operador`) VALUES ( '7', '11857435', '2016-04-11', '1460396502', 'ENTRADA', '2016-04-11', 'operador' );
INSERT INTO `tbinout`(`idinout`,`cedchkprof`,`diachkprof`,`horachkprof`,`registrochk`,`fechainout`,`operador`) VALUES ( '8', '12514148', '2016-04-11', '1460396512', 'ENTRADA', '2016-04-11', 'operador' );
INSERT INTO `tbinout`(`idinout`,`cedchkprof`,`diachkprof`,`horachkprof`,`registrochk`,`fechainout`,`operador`) VALUES ( '9', '19838394', '2016-04-11', '1460413816', 'SALIDA', '2016-04-11', 'operador' );
INSERT INTO `tbinout`(`idinout`,`cedchkprof`,`diachkprof`,`horachkprof`,`registrochk`,`fechainout`,`operador`) VALUES ( '10', '11857435', '2016-04-11', '1460413823', 'SALIDA', '2016-04-11', 'operador' );
INSERT INTO `tbinout`(`idinout`,`cedchkprof`,`diachkprof`,`horachkprof`,`registrochk`,`fechainout`,`operador`) VALUES ( '11', '12514148', '2016-04-11', '1460413830', 'SALIDA', '2016-04-11', 'operador' );
INSERT INTO `tbinout`(`idinout`,`cedchkprof`,`diachkprof`,`horachkprof`,`registrochk`,`fechainout`,`operador`) VALUES ( '12', '19838394', '2016-04-12', '1460482641', 'ENTRADA', '2016-04-12', 'operador' );
INSERT INTO `tbinout`(`idinout`,`cedchkprof`,`diachkprof`,`horachkprof`,`registrochk`,`fechainout`,`operador`) VALUES ( '13', '11857435', '2016-04-12', '1460482653', 'ENTRADA', '2016-04-12', 'operador' );
INSERT INTO `tbinout`(`idinout`,`cedchkprof`,`diachkprof`,`horachkprof`,`registrochk`,`fechainout`,`operador`) VALUES ( '14', '12514148', '2016-04-12', '1460482660', 'ENTRADA', '2016-04-12', 'operador' );
INSERT INTO `tbinout`(`idinout`,`cedchkprof`,`diachkprof`,`horachkprof`,`registrochk`,`fechainout`,`operador`) VALUES ( '15', '14279310', '2016-04-12', '1460482669', 'ENTRADA', '2016-04-12', 'operador' );
INSERT INTO `tbinout`(`idinout`,`cedchkprof`,`diachkprof`,`horachkprof`,`registrochk`,`fechainout`,`operador`) VALUES ( '16', '17335570', '2016-04-12', '1460482706', 'ENTRADA', '2016-04-12', 'operador' );
INSERT INTO `tbinout`(`idinout`,`cedchkprof`,`diachkprof`,`horachkprof`,`registrochk`,`fechainout`,`operador`) VALUES ( '17', '22251737', '2016-04-12', '1460482730', 'ENTRADA', '2016-04-12', 'operador' );
INSERT INTO `tbinout`(`idinout`,`cedchkprof`,`diachkprof`,`horachkprof`,`registrochk`,`fechainout`,`operador`) VALUES ( '18', '13414190', '2016-04-12', '1460482751', 'ENTRADA', '2016-04-12', 'operador' );
INSERT INTO `tbinout`(`idinout`,`cedchkprof`,`diachkprof`,`horachkprof`,`registrochk`,`fechainout`,`operador`) VALUES ( '19', '19838394', '2016-04-12', '1460500620', 'SALIDA', '2016-04-12', 'operador' );
INSERT INTO `tbinout`(`idinout`,`cedchkprof`,`diachkprof`,`horachkprof`,`registrochk`,`fechainout`,`operador`) VALUES ( '20', '19838394', '2016-05-17', '1463503352', 'ENTRADA', '2016-05-17', 'operador' );
INSERT INTO `tbinout`(`idinout`,`cedchkprof`,`diachkprof`,`horachkprof`,`registrochk`,`fechainout`,`operador`) VALUES ( '21', '13414190', '2016-09-08', '1473351677', 'ENTRADA', '2016-09-08', 'operador' );
INSERT INTO `tbinout`(`idinout`,`cedchkprof`,`diachkprof`,`horachkprof`,`registrochk`,`fechainout`,`operador`) VALUES ( '22', '11857435', '2017-01-17', '1484703480', 'ENTRADA', '2017-01-17', 'operador' );
-- ---------------------------------------------------------


-- Dump data of "tbprofe" ----------------------------------
INSERT INTO `tbprofe`(`idprof`,`cedulaprof`,`apenomprof`,`fotoprof`,`sexoprof`,`fnacprof`,`lugnacprof`,`correoprof`,`niveleducprof`,`cargoprof`,`gradoprof`,`materiaprof`,`tlfnocasprof`,`tlfnocelprof`,`direccionprof`,`statusprof`,`fechaprof`,`operador`) VALUES ( '1', '22251737', 'GUTIERREZ, MARIAVIRGINIA', '', 'FEMENINO', '1993-11-02', 'MARACAIBO', 'mvgf_123@hotmail.com', 'UNIVERSITARIO', 'MAESTRA DE AULA', 'MATERNAL A', 'COMPENDIO MATERNAL A', '0261 523-26-05', '0424 635-95-04', 'AV. LA POMONA CALLE 102 NRO CASA 18A-33', 'ACTIVO', '2016-01-12', 'admin' );
INSERT INTO `tbprofe`(`idprof`,`cedulaprof`,`apenomprof`,`fotoprof`,`sexoprof`,`fnacprof`,`lugnacprof`,`correoprof`,`niveleducprof`,`cargoprof`,`gradoprof`,`materiaprof`,`tlfnocasprof`,`tlfnocelprof`,`direccionprof`,`statusprof`,`fechaprof`,`operador`) VALUES ( '2', '13414190', 'GONZALEZ, KARINA', '', 'FEMENINO', '1975-11-07', 'MARACAIBO', 'JOHANKARI_1975@HOTMAIL.COM', 'UNIVERSITARIO', 'PROFESORA', 'SALA 5A', 'compendio sala 5a', '0424 609-07-38', '0416 113-89-00', 'SABANETA LARGA AV 100A CASA NRO 54-136B', 'ACTIVO', '2016-01-12', 'admin' );
INSERT INTO `tbprofe`(`idprof`,`cedulaprof`,`apenomprof`,`fotoprof`,`sexoprof`,`fnacprof`,`lugnacprof`,`correoprof`,`niveleducprof`,`cargoprof`,`gradoprof`,`materiaprof`,`tlfnocasprof`,`tlfnocelprof`,`direccionprof`,`statusprof`,`fechaprof`,`operador`) VALUES ( '3', '17335570', 'CALLEJAS, ARISBELYS', '', 'FEMENINO', '1986-05-19', 'MENE MAUROA', 'notiene@ceimiguelvazquez.edu.ve', 'TSU', 'MAESTRA DE AULA', 'SALA 4A', 'compendio sala 4a', '0261 418-71-22', '0424 602-21-16', 'AV 19D BARRIO LOS ANDES CASA NRO 110A-30', 'ACTIVO', '2016-01-12', 'admin' );
INSERT INTO `tbprofe`(`idprof`,`cedulaprof`,`apenomprof`,`fotoprof`,`sexoprof`,`fnacprof`,`lugnacprof`,`correoprof`,`niveleducprof`,`cargoprof`,`gradoprof`,`materiaprof`,`tlfnocasprof`,`tlfnocelprof`,`direccionprof`,`statusprof`,`fechaprof`,`operador`) VALUES ( '4', '17568626', 'MORALES, SUNAIRA', '', 'FEMENINO', '1984-08-15', 'MARACAIBO', 'sunairamorales@gmail.com', 'TSU', 'MAESTRA DE AULA', 'SALA 3A', 'compendio sala 3a', '1111 111-11-11', '0412 661-18-46', 'SECTOR POMONA CALLE 103 CON AV 19D CASA NRO 103-138', 'ACTIVO', '2016-01-12', 'admin' );
INSERT INTO `tbprofe`(`idprof`,`cedulaprof`,`apenomprof`,`fotoprof`,`sexoprof`,`fnacprof`,`lugnacprof`,`correoprof`,`niveleducprof`,`cargoprof`,`gradoprof`,`materiaprof`,`tlfnocasprof`,`tlfnocelprof`,`direccionprof`,`statusprof`,`fechaprof`,`operador`) VALUES ( '5', '12514148', 'ANDRADE, ROSELIN', '', 'FEMENINO', '1974-04-16', 'MARACAIBO', 'ROSELIN1629@HOTMAIL.COM', 'UNIVERSITARIO', 'MAESTRA DE AULA', 'SALA 5B', 'COMPENDIO SALA 5B', '0261 729-10-09', '0424 609-66-85', 'SABANETA CALLE 100 CON AV 21 CASA NOR 21A-28', 'ACTIVO', '2016-01-12', 'admin' );
INSERT INTO `tbprofe`(`idprof`,`cedulaprof`,`apenomprof`,`fotoprof`,`sexoprof`,`fnacprof`,`lugnacprof`,`correoprof`,`niveleducprof`,`cargoprof`,`gradoprof`,`materiaprof`,`tlfnocasprof`,`tlfnocelprof`,`direccionprof`,`statusprof`,`fechaprof`,`operador`) VALUES ( '6', '14279310', 'VILLALOBOS, DAYANA', '', 'FEMENINO', '1979-12-23', 'MARACAIBO', 'DAYANACAROLINA_32@HOTMAIL.ES', 'UNIVERSITARIO', 'MAESTRA DE AULA', 'MATERNAL B', 'COMPENDIO MATERNAL B', '0261 723-42-82', '0414 682-83-87', 'AV PRINCIPAL LA POMONA CALLE 102 CASA NOR 18-82', 'ACTIVO', '2016-01-12', 'admin' );
INSERT INTO `tbprofe`(`idprof`,`cedulaprof`,`apenomprof`,`fotoprof`,`sexoprof`,`fnacprof`,`lugnacprof`,`correoprof`,`niveleducprof`,`cargoprof`,`gradoprof`,`materiaprof`,`tlfnocasprof`,`tlfnocelprof`,`direccionprof`,`statusprof`,`fechaprof`,`operador`) VALUES ( '7', '11857435', 'ARAUJO, KEINNY', '', 'FEMENINO', '1973-10-18', 'MARACAIBO', 'notiene@ceimiguelvazquez.edu.ve', 'TSU', 'MAESTRA DE AULA', 'SALA 4B', 'COMPENDIO SALA 4B', '0261 723-71-37', '0424 611-22-72', 'CALLE 105 CON AV 19B CASA NOR 105-37', 'ACTIVO', '2016-01-12', 'admin' );
INSERT INTO `tbprofe`(`idprof`,`cedulaprof`,`apenomprof`,`fotoprof`,`sexoprof`,`fnacprof`,`lugnacprof`,`correoprof`,`niveleducprof`,`cargoprof`,`gradoprof`,`materiaprof`,`tlfnocasprof`,`tlfnocelprof`,`direccionprof`,`statusprof`,`fechaprof`,`operador`) VALUES ( '8', '19838394', 'CONTRERAS, ROSSY', '', 'FEMENINO', '1990-09-25', 'MARACAIBO', 'contrerasrossy@hotmail.com', 'UNIVERSITARIO', 'MAESTRA DE AULA', 'SALA 3B', 'COMPENDIO SALA 3B', '0261 736-49-51', '0414 367-09-99', 'URB EL PINAR PINO CENTRO 1 APTO 2F SECTOR POMONA', 'ACTIVO', '2016-01-12', 'admin' );
-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


